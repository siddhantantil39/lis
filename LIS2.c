#include <stdio.h>
#include<string.h>
#include <stdlib.h>

int n;
int Sequence[32];
int L[32];
int LisSequence[32];
void takeInput()
{
    printf("\n Enter the length");
    scanf("%d",&n);

    printf("\n Enter the Sequence");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&Sequence[i]);
    }
}


int LIS()
{
    int i,j;

    for(i=0;i<n;i++)
    {
        L[i] = 1;
    }
    for(i=0;i<n;i++)
    {

        for(j=i+1;j<n;j++ )
        {
            if(Sequence[j]>Sequence[i])
            {
                if(L[j]<L[i]+1)
                    L[j]=L[i]+1;
            }
        }
    }

    int maxLength=0;
    for(i=0;i<n;i++)
    {
        if(maxLength<L[i])
            maxLength=L[i];
    }

    return maxLength;
}

void findSequence(int maxLength)
{
    int i,j;
    i=0;
    for(j=1;j<n;j++)
    {
        if(L[j]>L[i])
            i=j;
    }

    int top=L[i]-1;

    LisSequence[top]=Sequence[i];
    top--;
    for(j=i-1;j>=0;j--)
    {
        if(Sequence[j]<Sequence[i]&&L[j]==L[i]-1)
        {
            i=j;
            LisSequence[top]=Sequence[i];
            top--;
        }
    }

    printf("LIS is");
    for(i=0;i<maxLength;i++) {
        printf(" %d",LisSequence[i]);
    }
    puts("");
}


int main() {
    takeInput();
    int result=LIS();
    printf("The LIS length is %d\n", result);
    findSequence(result);
    return 0;
}